## opensteck安装

###### 基础配置

```bash
cat /etc/sysconfig/network-scripts/ifcfg-eno16777736		
TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
NAME=eno16777736
DEVICE=eno16777736
ONBOOT=yes
IPADDR=192.168.10.10
NETMASK=255.255.255.0

（2）上传基础镜像

上传XianDian-IaaS-v2.2.iso和CentOS-7-x86_64-DVD-1511.iso两个镜像包至controller节点/root目录中，并将ISO文件挂载至/opt/目录中。

 
[root@localhost ~]# mkdir /opt/centos
[root@localhost ~]# mkdir /opt/iaas
[root@localhost ~]# mount /root/CentOS-7-x86_64-DVD-1511.iso /opt/centos7.2/
[root@localhost ~]# mount /root/XianDian-IaaS-v2.2.iso /opt/iaas/

（3）配置YUM源文件

controller节点：

[root@localhost ~]# rm -rf /etc/yum.repos.d/CentOS-*

[root@localhost ~]#


\> [centos]
\> name=centos
\> baseurl=file:///opt/centos7.2
\> gpgcheck=0
\> enabled=1
\> [iaas]
\> name=iaas
\> baseurl=file:///opt/iaas/iaas-repo
\> gpgcheck=0
\> enabled=1
\> EOF

compute节点：

[root@localhost ~]# rm -rf /etc/yum.repos.d/CentOS-*

[root@localhost ~]# cat > /etc/yum.repos.d/local.repo <<EOF

\> [centos]

\> name=centos

\> baseurl=ftp://192.168.10.10/centos7.2

\> gpgcheck=0

\> enabled=1

\> [iaas]

\> name=iaas

\> baseurl=ftp://192.168.10.10/iaas/iaas-repo

\> gpgcheck=0

\> enabled=1

\> EOF

（4）控制节点安装ftp服务

控制节点安装vsftpd服务，提供计算节点ftp访问方式。

[root@localhost ~]# yum install vsftpd -y

在/etc/vsftpd/vsftpd.conf配置中添加一行代码：

anon_root=/opt

重启vsftpd服务：

[root@localhost ~]# systemctl restart vsftpd

（5）配置防火墙策略

在控制节点和计算节点中关闭防火墙：

[root@localhost ~]# setenforce 0

[root@localhost ~]# iptables -F

[root@localhost ~]# iptables -X

[root@localhost ~]# iptables -Z

[root@localhost ~]# systemctl stop firewalld
```

###### 脚本安装

```bash
yum install iaas-xiandian -y	#在控制节点和计算节点安装iaas-xiandian服务

vi /etc/xiandian/openrc.sh		#控制节点和计算节点配置环境变量的配置文件,配置参数说明如下：
:%s/^#/		#去除第一个#
HOST_IP=192.168.10.10
HOST_NAME=controller
HOST_IP_NODE=192.168.10.20
HOST_NAME_NODE=compute
RABBIT_USER=openstack
RABBIT_PASS=000000
DB_PASS=000000
DOMAIN_NAME=demo
ADMIN_PASS=000000
DEMO_PASS=000000
KEYSTONE_DBPASS=000000
GLANCE_DBPASS=000000
GLANCE_PASS=000000
NOVA_DBPASS=000000
NOVA_PASS=000000
NEUTRON_DBPASS=000000
NEUTRON_PASS=000000
METADATA_SECRET=000000
INTERFACE_NAME=eno33554960
\##节点第二块网卡名称
CINDER_DBPASS=000000
CINDER_PASS=000000
BLOCK_DISK=sda3
\##计算节点cinder服务使用空分区
SWIFT_PASS=000000
OBJECT_DISK=sda4
\##计算节点swift服务使用空分区
STORAGE_LOCAL_NET_IP=192.168.10.20		#计算节点地址

iaas-pre-host.sh		#控制节点和计算节点安装基础服务
reboot		#重启

（9）安装MySQL数据库服务
控制节点通过脚本安装MySQL数据库服务：
iaas-install-mysql.sh		#控制节点安装MySQL数据库服务

（10）安装Keystone认证服务
iaas-install-keystone.sh		#控制节点安装Keystone认证服

（11）安装Glance镜像服务
iaas-install-glance.sh		#控制节点安装Glance镜像服务

（12）安装Nova计算服务
iaas-install-nova-controller.sh		#controller节点安装计算服务
iaas-install-nova-compute.sh		#compute节点安装计算服务

（13）安装Neutron网络服务
iaas-install-neutron-controller.sh		#controller节点安装网络服务
iaas-install-neutron-controller-gre.sh
iaas-install-neutron-compute.sh		#compute节点安装网络服务
iaas-install-neutron-compute-gre.sh

（14）安装Dashboard服务
iaas-install-dashboard.sh		#controller节点安装Dashboard服务

（15）安装Cinder块存储服务
iaas-install-cinder-controller.sh		#controller节点安装块存储服务
iaas-install-cinder-compute.sh		#compute节点安装块存储服务

（16）安装Swift对象存储服务
iaas-install-swift-controller.sh		#controller节点安装对象存储服务
iaas-install-swift-compute.sh		#compute节点安装对象存储服务

（17）访问Dashboard服务
打开浏览器，访问http://192.168.10.10/dashboard地址，输入环境变量文件中填写的密码，域为demo、用户名为admin、密码为000000，然后单击“连接”按钮。
```

## openstask运维

```bash
openstack-service status
openstack-service status | grep failed		#会发现会有失败的服务，所以需要重启
openstack-service restart
openstack-service status | grep neutron		
#可能是因为neutron导致
systemctl restart httpd memcached		#重启一下httpd和memcached服务
```

###### Keystone服务运维

```bash
source /etc/keystone/admin-openrc.sh

openstack user create --password 000000 --email alice@example.com --domain demo alice		#创建用户
openstack user list		#查询所有用户
openstack user show alice		#查询单个用户的详细信息

openstack project create --domain demo acme		#创建项目
openstack project list		#查询所有项目
openstack project show acme		#查询单个项目的详细信息

openstack role create compute-user		#创建角色
openstack role list		#查询所有角色
openstack role show compute-user		#查询单个角色的详细信息

openstack role add --user alice --project acme compute-user		#绑定用户和项目权限
openstack endpoint list		#Keystone组件管理OpenStack平台中所有服务端点信息，通过命令可以查询平台中所有服务所使用的端点地址信息，命令如下：
```

###### Glance服务运维

```bash
glance image-create --name "cirros" --disk-format qcow2 --container-format bare
--progress < /root/cirros-0.3.3-x86_64-disk.img		#创建镜像
glance image-list		#查看镜像列表
glance image-show eee2434a-8dc8-42c3-b9a4-fd8dcdebf6f3	#查询单个镜像的详细信息
glance image-update --min-disk=1 eee2434a-8dc8-42c3-b9a4-fd8dcdebf6f3		#更改镜像
glance image-delete eee2434a-8dc8-42c3-b9a4-fd8dcdebf6f3解决居民		#删除镜像
```

###### Nova服务运维

```bash
nova secgroup-create test 'test the nova command about the rules'		#创建一个名为test的安全组，描述为'test the nova command about the rules'
nova flavor-create test 6 2048 20 2		#创建一个名为test，ID为6，内存为2048 MB，磁盘为20 GB，vCPU数量为2的云主机类型
nova flavor-show test		#查看test云主机类型的详细信息
nova secgroup-list		#查看安全组
nova secgroup-list-rules default		#查看安全组内的规则
nova secgroup-add-rule test udp 1 65535 0.0.0.0/0		#添加安全组规则
nova secgroup-add-rule test tcp 1 65535 0.0.0.0/0
nova secgroup-add-rule test icmp -1 -1 0.0.0.0/0
nova secgroup-delete secGroupName		#删除安全组

创建网络：
外网：
neutron net-create public --router:external=True
子网：
neutron subnet-create --gateway 192.168.200.2 --enable-dhcp --name out-sub public 192.168.200.0/24
内网：
neutron net-create int-net
子网
neutron subnet-create --gateway 172.16.1.1 --enable-dhcp --dns-nameserver 8.8.8.8 --name in-sub int-net 172.16.1.0/24
neutron router-create route2		#创建路由
neutron router-gateway-set route2 public		#连接外网
neutron router-interface-add route2 in-sub		#连接内网
neutron router-list		#删除路由
neutron router-gateway-clear a38d5c58-f0b6-4a82-8b13-a0d7f2d22d04		#清除外网网关
neutron router-port-list a38d5c58-f0b6-4a82-8b13-a0d7f2d22d04		#清除内网子网接口
neutron router-interface-delete a38d5c58-f0b6-4a82-8b13-a0d7f2d22d04 59707a32-
f4c4-4247-80c0-8db5bf3bdc76
neutron router-delete route2		#清除路由
==================================
```

###### Neutron运维

```bash
neutron agent-list		#查询网络服务DHCP agent的详细信息（id为查询到DHCP agent服务对应id）
neutron agent-list -c binary		#查询网络服务的列表信息中的“binary”一列
neutron net-list		#查看网络详细信息
neutron net-show da32913c-5ada-4210-bdb2-6949d3a45f94
neutron subnet-list
neutron subnet-show 46f50502-4e35-457c-ae58-3e781608a350


```

###### Cinder服务运维

```bash
cinder create --name extend-demo 2		#创建一个2GB的云硬盘extend-demo
cinder list		#查看云硬盘信息
cinder type-create lvm		#创建云硬盘卷类型
 cinder create --name type_test_demo --volume-type lvm 1		#创建带标识云硬盘
cinder show type_test_demo		#查看 该卷的volume_type字段已修改为“lvm”
cinder delete cinder-demo		#删除指定的Cinder卷
```

###### Swift服务运维

```bash
swift post test		#创建容器
swift list test		#查询容器
swift upload test file/		#上传文件至容器
swift upload test/file one.txt
swift download test file/three.png		#下载文件
swift delete test file/three.png		#删除文件
swift stat		#查看容器服务状态

```

###### 创建云主机

```
nova boot --flavor test --image cirros --security-group test --nic net-name=int-net server
nova get-vnc-console server novnc

cinder create --display-name vol1 1 添加云硬盘 nova volume-attach server 8c6a585c-9206-41f6-8316-4c76e0d0c475

nova floating-ip-create public		#绑定浮动IP
nova floating-ip-associate server 20.0.0.4 
nova get-vnc-console server novnc
```

