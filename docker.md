Docker安装

```bash
tar -zxvf Docker.tar.gz -C /opt
cat /etc/yum.repos.d/local.repo
[kubernetes]
name=kubernetes
baseurl=file:///root/Docker
gpgcheck=0
enabled=1

yum upgrade -y		#升级系统内核
uname -r 

cat >> /etc/sysctl.conf << EOF		#开启路由转发
net.ipv4.ip_forward=1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl -p



yum install -y yum-utils device-mapper-persistent-data		#yum-utils提供了yum-config-manager的依赖包，device-mapper-persistent-data和lvm2are需要devicemapper存储驱动
yum install docker-ce-18.09.6 docker-ce-cli-18.09.6 containerd.io -y		#处安装指定版本的Docker CE

systemctl daemon-reload;systemctl restart docker;systemctl enable docker		#启动Docker并设置开机自启
docker info		#查看Docker的系统信息
```
##### （1）运行Registry

将Registry镜像运行并生成一个容器。

```
[root@master ~]# ./image.sh

[root@master ~]# docker run -d -v /opt/registry:/var/lib/registry -p 5000:5000 --restart=always --name registry registry:latest

1ce68b8a0bdec1465e6e75b88e19e65d20609455baaa9d45b8d93c9fdca9b24e

##### 
```

##### （2）查看运行情况

使用docker ps命令查看容器运行情况：

```
[root@master ~]# docker ps

CONTAINER ID    IMAGE        COMMAND         CREATED       STATUS       PORTS          NAMES

1ce68b8a0bde    registry:latest   "/entrypoint.sh /etc…"  4 minutes ago    Up 4 minutes    0.0.0.0:5000->5000/tcp  registry
```



##### （3）查看状态

Registry容器启动后，打开浏览器输入地址http://ip_add:5000/v2/

##### （4）上传镜像

创建好私有仓库之后，就可以使用docker tag命令来标记一个镜像，然后推送它到仓库。

先配置私有仓库，示例代码如下：

```
[root@master ~]# cat /etc/docker/daemon.json 

{

 "insecure-registries": ["10.18.4.30:5000"]

}

[root@master ~]# systemctl restart docker
```

使用docker tag命令将centos:latest这个镜像标记为10.18.4.30:5000/centos:latest。

[root@master ~]#

```
 docker tag centos:latest 10.18.4.30:5000/centos:latest
```

docker tag语法如下：

```
\# docker tag IMAGE:TAG/]REPOSITORY[:TAG]
```

使用docker push上传标记的镜像：

```
[root@master ~]# docker push 10.18.4.30:5000/centos:latest

The push refers to repository [10.18.4.30:5000/centos]

9e607bb861a7: Pushed 

latest: digest: sha256:6ab380c5a5acf71c1b6660d645d2cd79cc8ce91b38e0352cbf9561e050427baf size: 529
```

使用curl命令查看仓库中的镜像：

```
[root@master ~]# 

{"repositories":["centos"]}
```

如同上述代码所示，提示{"repositories":["centos"]}，则表明镜像已经上传成功了。

##### （5）拉取镜像

登录Slave节点，配置私有仓库地址。

```
[root@slave ~]# cat /etc/docker/daemon.json 

{

 "insecure-registries": ["10.18.4.30:5000"]

}

[root@slave ~]# systemctl restart docker
```

拉取镜像并查看结果。

```
[root@slave ~]# docker pull 10.18.4.30:5000/centos:latest

latest: Pulling from centos

729ec3a6ada3: Pull complete 

Digest: sha256:6ab380c5a5acf71c1b6660d645d2cd79cc8ce91b38e0352cbf9561e050427baf

Status: Downloaded newer image for 10.18.4.30:5000/centos:latest

[root@slave ~]# docker images

REPOSITORY         TAG    IMAGE ID    CREATED   SIZE

10.18.4.30:5000/centos      latest   0f3e07c0138f   4 weeks ago  220MB
```

​                                   

